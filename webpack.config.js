const fs = require('fs');
const readline = require('readline');
const path = require('path');

module.exports = {
  entry: './src/index.ts',
  mode: "development",
  target: 'node',
  node: {
    fs: true,
    readline: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};
