export default class CapacityOverflowException extends Error {
  constructor() {
    const msg = 'Maximum Capacity of our store was reached!!!';
    super(msg);
    Error.captureStackTrace(this);
    this.name = 'CapacityOverflowException';
    this.message = msg;
  }
}
