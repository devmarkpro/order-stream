export default class InvalidLineException extends Error {
  constructor(message: string | undefined, ...params: string[]) {
    let msg: string = message
      ? message + ' => ' + ' Cannot parse line: '
      : 'Cannot parse line: ';
    params.forEach((p: string) => (msg += p + ' -'));
    super(msg);
    Error.captureStackTrace(this);
    this.name = 'InvalidLineException';
    this.message = msg;
  }
}
