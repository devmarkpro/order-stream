import { getCommands } from './CommandHelper';
import { IllegalArgumentException } from '../Errors';

const commands = ['ts-node', 'index.ts'];
describe('*** parse input params', () => {
  it('should have --input param', () => {
    expect(() => getCommands(commands)).toThrowError(IllegalArgumentException);
  });
  it('should get input parameter', () => {
    const path = './somewhere';
    commands.push('--input', path);
    const result = getCommands(commands);
    expect(result).toHaveProperty('input');
    expect(result.input).toMatch(path);
  });
});
