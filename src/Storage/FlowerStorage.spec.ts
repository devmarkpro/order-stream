import { FlowerStorage } from './FlowerStorage';
const storage = new FlowerStorage();
describe('*** flower storage', () => {
  beforeEach(() => storage.clear());
  it('should add item to flower storage', () => {
    storage.add({
      id: 'test:l',
      name: 'test',
      size: 'l',
    });
    expect(storage.getCount()).toEqual(1);
  });
  it('should clear flower storage', () => {
    storage
      .add({
        id: 'test:l',
        name: 'test',
        size: 'l',
      })
      .clear();
    expect(storage.getCount()).toEqual(0);
  });
  it('should get item from storage', () => {
    storage.add({
      id: 'test:l',
      name: 'test',
      size: 'l',
    });
    const items = storage.get();
    expect(items[0].count).toEqual(1);
    expect(items[0].flower.id).toEqual('test:l');
  });
});
