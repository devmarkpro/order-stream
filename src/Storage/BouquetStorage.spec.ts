import { BouquetStorage } from './BouquetStorage';
import { Bouquet } from '../Models/Bouquet';
const storage = new BouquetStorage();
describe('*** bouquet storage', () => {
  beforeEach(() => storage.clear());
  it('should add item to bouquet storage', () => {
    storage.add({
      id: 'test:l',
      name: 'test',
      size: 'l',
      spec: 'test',
      flowers: [],
    });
    expect(storage.getCount()).toEqual(1);
  });

  it('should clear flower storage', () => {
    storage
      .add({
        id: 'test:l',
        name: 'test',
        size: 'l',
        spec: 'test',
        flowers: [],
      })
      .clear();
    expect(storage.getCount()).toEqual(0);
  });

  it('should get item from storage', () => {
    storage.add({
      id: 'test:l',
      name: 'test',
      size: 'l',
      spec: 'test',
      flowers: [],
    });
    const items = storage.get();
    expect(
      items.filter((a: Bouquet) => a.name === 'test' && a.size === 'l').length,
    ).toEqual(1);
  });

  it('should remove item from storage', () => {
    const b1 = new Bouquet('test', 'test', 'l', []);
    const b2 = new Bouquet('test1', 'test1', 's', []);
    storage.add(b1).add(b2);
    storage.remove(b1);
    const items = storage.get();
    expect(
      items.filter((a: Bouquet) => a.name === 'test1' && a.size === 's').length,
    ).toEqual(1);
  });
});
