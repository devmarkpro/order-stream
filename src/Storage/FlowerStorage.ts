import FlowerQuantity from 'FlowerCount';
import { Flower } from '../Models/Flower';
import { MAX_FLOWERS_PROCESS_CAPACITY } from '../Config';
import CapacityOverflowException from '../Errors/CapacityOverflowException';
import { Bouquet } from '../Models/Bouquet';

let flowers: FlowerQuantity[] = [];
/**
 * Flower storage
 */
export class FlowerStorage {
  /**
   * Adds flower storage
   * @param {Flower} flower
   * @param {number} [count=1]
   * @returns {FlowerStorage}
   */
  public add(flower: Flower, count: number = 1): FlowerStorage {
    if (this.getCount() >= MAX_FLOWERS_PROCESS_CAPACITY) {
      throw new CapacityOverflowException();
    }
    const current = flowers.findIndex(
      (a: FlowerQuantity) => a.flower.id === flower.id,
    );
    if (current >= 0) {
      flowers[current].count += count;
    } else {
      flowers.push({
        flower,
        count,
      });
    }
    return this;
  }

  /**
   * Get  of flower storage
   * @returns {Array<FlowerQuantity>}
   */
  public get = (): FlowerQuantity[] => {
    return flowers;
  };
  /**
   * Clear all flower from storage
   */
  public clear = () => {
    flowers = [];
  };
  /**
   * Get count of flower storage
   * @return {number}
   */
  public getCount = (): number => {
    if (flowers.length === 0) {
      return 0;
    }
    return flowers
      .map((flower: FlowerQuantity) => flower.count)
      .reduce((total: number, count: number) => total + count);
  };
  /**
   * Remove all the flowers of the bouquet from the flower storage
   * @param {Bouquet} bouquet
   */
  public closeBouquet = (bouquet: Bouquet): void => {
    bouquet.flowers.forEach((flowerQuantity: FlowerQuantity) => {
      const index = flowers.findIndex(
        a =>
          a.flower.id.toLowerCase() ===
            flowerQuantity.flower.id.toLowerCase() &&
          flowerQuantity.flower.name.toLowerCase() !== 'other',
      );
      if (index >= 0) {
        flowers[index].count -= flowerQuantity.count;
      }
    });
  };
}
