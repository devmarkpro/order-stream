import { Flower } from './Flower';
import { InvalidLineException } from '../Errors';
import FlowerQuantity from 'FlowerCount';

/**
 * Bouquet
 */
export class Bouquet {
  /**
   * Parses bouquet Convert string to a Bouquet object
   * @param {string} phrase . the input phrase
   * @returns {Bouquet|null} Bouquet object based on the input string
   */
  public static Parse(phrase: string): Bouquet | null {
    if (!phrase || phrase.length === 0) {
      return null;
    }
    // Get all the characters from the input string
    const chars: string[] = phrase.match(Bouquet.charMatchRegex)!;

    // Get all the numbers from the input string
    const numbers: string[] = phrase.match(Bouquet.numberMatchRegex)!;
    if (!chars || !numbers) {
      throw new InvalidLineException(
        'Cannot get size or name of the Bouquet',
        phrase,
      );
    }
    // Get and Remove the first item of chars which indicate name of the Bouquet
    const name = chars.shift();
    // Get and Remove the first item of chars which indicate Size of the Bouquet
    const size = chars.shift();
    if (!name || !size) {
      throw new InvalidLineException(
        'Cannot get size or name of the Bouquet',
        phrase,
      );
    }
    const flowerQuantities: FlowerQuantity[] = [];
    chars.forEach((char: string, index: number) => {
      flowerQuantities.push({
        flower: new Flower(chars[index], size.toLowerCase()),
        count: parseInt(numbers[index], 10),
      });
    });
    flowerQuantities.push({
      flower: new Flower('Other', size.toLowerCase()),
      count: Number(numbers[numbers.length - 1]),
    });

    return new Bouquet(phrase, name, size, flowerQuantities);
  }
  private static charMatchRegex = /[a-zA-Z]/g;
  private static numberMatchRegex = /\d+/g;
  public id: string;
  public spec: string;
  public name: string;
  public size: string;
  public flowers: FlowerQuantity[];

  /**
   *
   * @param {string} spec the spec of the bouquet which got from the input
   * @param {string} name  the name of the bouquet
   * @param {string} size  the size of the bouquet
   * @param {Array<FlowerQuantity>} flowers flower quantity which show each flower with it's quantity in this bouquet
   */
  constructor(
    spec: string,
    name: string,
    size: string,
    flowers: FlowerQuantity[] = [],
  ) {
    this.id = `${name}:${size}`;
    this.spec = spec;
    this.name = name;
    this.size = size;
    this.spec = spec;
    this.flowers = flowers;
  }
}
