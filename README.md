# Order Stream
## Build and Run

This project is based on *Typescript* so you can run the project after build it or run directory from typescript by `ts-node`

- `yarn` or `npm install` to install all dependencies
- run `yarn start` to build and start the project
- Alternatively you can start project via `yarn dev` to run project directly in *Typescript*
- pass `--input` param to send the input file to the application eg: `yarn start --input ../input.txt`

## Tests

For run the test simply run `yarn test` command. you can see the html coverage in the folder called `coverage` in the root of the project.

## Docker

The application is dockerize! so you can simply build and run the docker image.

### Build image by yourself

- `yarn install && yarn build` to install dependencies and build project
- `docker build -t order-stream .` to create docker image from project
- `docker run -v ~/Desktop/inputs:/data order-stream` to run and send your input file


### TODO

The list of works that I can do if I had more time.

- [ ] Add more test.
- [ ] Better commenting.
- [ ] Add log verbosity as input param.
- [ ] Add more log based on verbosity.
