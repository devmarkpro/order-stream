import { Flower } from '../src/Models/Flower';
export default interface FlowerCount {
  flower: Flower;
  count: number;
}
